<?php
/* Navegation.php 
*  Permite la navegacion entre las vistas.
*  2020/01/24
*/
class Navegation
{

  static function returnView($url)
  {
    if (
      $url == "iniciar" ||
      $url == "salir" || 
      $url == "recuperar" ||
      $url == "admin" ||
      $url == "gestor" ||
      $url == "inicio" ||
      $url == "perfil" ||
      $url == "invitacion" ||
      $url == "carrito" ||
      $url == "articulo"
    ) {
      $view = "view/" . $url . ".php";
    } else {
      $view = "view/error.php";
    }
    return $view;
  }
}
