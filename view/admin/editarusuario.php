<!DOCTYPE html>
<html lang="es">

<head>
  <?php include "view/head/head.php"; ?>
  <title>Editar usuario </title>
</head>


<body>
<?php include "view/head/nav.php"; ?>
<main class="row">
  <div class="col s12 m10 l8 xl6 offset-m1 offset-l2 offset-xl3">
    <div class="card-panel">
      <h3 class="center">Editar usuario</h3>
      <form action="POST" class="container">
        <div class="row">
          <div class="input-field col s12">
            <input type="text" name="nom" id="nom" class="validate">
            <label for="nom">Nombre completo</label>
          </div>
          <div class="input-field col s12">
            <input type="email" name="email" id="email" class="validate">
            <label for="email">Correo</label>
            <span class="helper-text" data-error="utiliza tu correo xcaret.com" data-success="correo verificado">Escribe tu correo institucional</span>
          </div>
          <div class="input-field col s12">
            <input type="text" name="num" id="num" class="validate" step="2">
            <label for="num">Número de colaborador</label>
           
          </div>
          <div class="input-field col s12">
            <input type="tel" name="tel" id="tel" class="validate">
            <label for="tel">Teléfono</label>
          </div>
          <div class="col s12 m6">
          <select>
      <option value="" disabled selected>Seleccione UDN</option>
      <option value="1">Xcaret</option>
      <option value="2">Xel-Há</option>
      <option value="3">Xplor</option>
    </select>
    <label>Materialize Select</label>
          </div>
          
          <div class="col s12 m6">
          <select>
      <option value="" disabled selected>Seleccione tipo de usuario</option>
      <option value="1">Administrador</option>
      <option value="2">Gestor</option>
      <option value="3">Colaborador</option>
    </select>
    <label>Materialize Select</label>
          </div>
        </div>
        <div class="row">
          <div class="col s12 ">
            <button type="submit" class="btn btn-large waves-effect waves-light right ">Actualizar</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</main>
  
  <?php include "view/foot/foot.php"; ?>
</body>

</html>