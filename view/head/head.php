    <meta charset="UTF-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
    <meta name="description" content="Xubastas" />
    <meta name="keywords" content="Subasta, Subastas" />
    <meta name="author" content="Soporte XH Team" />
    <!--Color Tab on Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#00675b" />
    <meta name="msapplication-TileColor" content="#00675b">
    <!-- <meta name="msapplication-TileImage" content="<?php //echo WEBHOST . "view/img/favicon/ms-icon-144x144.png"; ?>"> -->
    <meta name="msapplication-tap-highlight" content="no">
    <!--Color Tab on Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#00675b" />
    <!--Color Tab on iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#00675b" />
    <!--Favicon -->
    <!--<link rel="apple-touch-icon" sizes="57x57" href="<?php //echo WEBHOST . "view/img/favicon/apple-icon-57x57.png"; ?>">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php //echo WEBHOST . "view/img/favicon/apple-icon-60x60.png"; ?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php //echo WEBHOST . "view/img/favicon/apple-icon-72x72.png"; ?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php //echo WEBHOST . "view/img/favicon/apple-icon-76x76.png"; ?>v">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php //echo WEBHOST . "view/img/favicon/apple-icon-114x114.png"; ?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php //echo WEBHOST . "view/img/favicon/apple-icon-120x120.png"; ?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php //echo WEBHOST . "view/img/favicon/apple-icon-144x144.png"; ?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php //echo WEBHOST . "view/img/favicon/apple-icon-152x152.png"; ?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php //echo WEBHOST . "view/img/favicon/apple-icon-180x180.png"; ?>">
    <link rel="icon" type="image/png" sizes="192x192" href="<?php //echo WEBHOST . "view/img/favicon/android-icon-192x192.png"; ?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php //echo WEBHOST . "view/img/favicon/favicon-32x32.png"; ?>">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php //echo WEBHOST . "view/img/favicon/favicon-96x96.png"; ?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php //echo WEBHOST . "view/img/favicon/favicon-16x16.png"; ?>">
    <link rel="manifest" href="<?php //echo WEBHOST . "view/img/favicon/manifest.json"; ?>">-->
    <!-- Styles -->
    <link rel="stylesheet" href="<?php echo WEBHOST . "view/css/material.css"; ?>" as=" style" media="all" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,600&display=swap">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">