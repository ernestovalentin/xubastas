<!DOCTYPE html>
<html lang="es">

<head>
  <?php include "view/head/head.php"; ?>
  <title>Iniciar Sesión</title>
</head>

<body>
<?php include "view/head/nav.php"; ?>
<main class="row" style="margin-top:100px;">
  <div class="col s12 m10 l8 xl6 offset-m1 offset-l2 offset-xl3">
    <div class="card-panel">
      <h1 class="center">Iniciar Sesión</h1>
      <form action="POST" class="container">
        <div class="row">
          <div class="input-field col s12">
            <input type="email" name="email" id="email" class="validate">
            <label for="email">Correo</label>
            <span class="helper-text" data-error="utiliza tu correo xcaret.com" data-success="correo verificado">Escribe tu correo institucional</span>
          </div>
          <div class="input-field col s12">
            <input type="password" name="password" id="password" class="validate">
            <label for="password">Contraseña</label>
          </div>
          <div class="col s12">
            <p class="center">
              <label>
                <input type="checkbox" name="remember" id="remember" checked="checked" />
                <span>Recordarme</span>
              </label>
            </p>
          </div>
        </div>
        <div class="row">
          <div class="col s12 center">
            <button type="submit" class="btn btn-large waves-effect waves-light">Iniciar Sesión</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</main>
  
  <?php include "view/foot/foot.php"; ?>
</body>

</html>