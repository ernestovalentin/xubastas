<?php
/* Main.php
* Clase de inicio para la aplicacion
* 2020/01/24
*/
require_once "controlador/Navegation.php";

class Main
{
  public $url = array(); //Crea una variable que guardara la direccion escrita en el navegador
  function __construct()
  {
    //Se definen variables globales que se usaran en toda la aplicacion
    define("DBHOST", "xhsistickets");
    define("DBNAME", "xubastas");
    define("DBUSER", "xubastas");
    define("DBPASSWORD", "Xubastas2020");
    define("WEBHOST", "//localhost/xubastas/");

    $this->callView();
  }

  private function callView()
  {
    if (isset($_GET["v"])) {
      $url = explode("/", $_GET["v"]);
    } else {
      $url[0] = "iniciar";
    }
    include Navegation::returnView($url[0]);
  }
}
